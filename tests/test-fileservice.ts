import { FileService } from '../src/file.service';
import { logger } from './test-logger';

export class TestFileService extends FileService implements FileService {
  public operations: {
    method: 'write' | 'read' | 'delete' | 'exists' | 'ensureExists';
    args: any[];
  }[] = [];

  public async write<TSchema>(path: string, data: TSchema): Promise<void> {
    this.operations.push({
      method: 'write',
      args: [path, data],
    });
  }
  public async read<TSchema>(path: string, defaultValue?: TSchema, result?: TSchema): Promise<TSchema> {
    this.operations.push({
      method: 'read',
      args: [path, defaultValue],
    });

    return result;
  }
  public async delete(path: string): Promise<void> {
    this.operations.push({
      method: 'delete',
      args: [path],
    });
  }
  public exists(path: string, result?: boolean): boolean {
    this.operations.push({
      method: 'exists',
      args: [path],
    });

    return result;
  }
  public async ensureExists<TSchema>(path: string, defaultValue?: TSchema): Promise<void> {
    this.operations.push({
      method: 'ensureExists',
      args: [path, defaultValue],
    });
  }
}

export const testFileService = new TestFileService('', logger);
