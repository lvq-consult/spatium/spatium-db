import { existsSync, readFile, readJson, remove } from 'fs-extra';
import { cloneDeep, range } from 'lodash';
import { resolve } from 'path';
import { AfterAll, BeforeAll, expect, Test, TestSuite, Timeout } from 'testyts';
import { SpatiumDatabase, SpatiumEntity, SPATIUM_DATE_TRANSFORMER } from '../src';
import { IndexFile } from '../src/index-handler';
import { TestDatabase } from './test-database';
import { logger } from './test-logger';

@TestSuite()
export class DatabaseTests {
  private tmpFolder = resolve(process.cwd(), './tmp');
  private integrationDataFolder = resolve(this.tmpFolder, './integration');
  private exampleDataFolder = resolve(this.tmpFolder, './example');
  private benchmarkFolder = resolve(this.tmpFolder, './benchmark');

  @BeforeAll()
  async beforeAll() {
    if (existsSync(this.tmpFolder)) {
      await remove(this.tmpFolder);
    }
  }

  @AfterAll()
  async afterAll() {
    if (existsSync(this.tmpFolder)) {
      await remove(this.tmpFolder);
    }
  }

  @Test('should create new instance')
  createInstance() {
    const db = new TestDatabase();

    expect.toBeDefined(db);
  }

  @Test('complete integration test')
  async integrationTest() {
    interface TestEntity1 extends SpatiumEntity {
      test: string;
    }

    interface TestEntity2 extends SpatiumEntity {
      test: number;
    }

    interface TestEntity3 extends SpatiumEntity {
      test: boolean;
      date: Date;
    }

    const dataFolder = this.integrationDataFolder;

    const integrationDb = class extends SpatiumDatabase {
      public testEntities1 = this.getTable<TestEntity1>('testEntity1', ['id', 'test']);
      public testEntities2 = this.getTable<TestEntity2>('testEntity2', ['id', 'test']);
      public testEntities3 = this.getTable<TestEntity3>('testEntity3', ['id', 'test', 'date'], {
        date: SPATIUM_DATE_TRANSFORMER,
      });

      constructor() {
        super(dataFolder, logger);
      }
    };
    const db = new integrationDb();

    await db.ensureTables();

    const testEntity1IndexPath = resolve(this.integrationDataFolder, 'testEntity1', 'index.json');
    const testEntity2IndexPath = resolve(this.integrationDataFolder, 'testEntity2', 'index.json');
    const testEntity3IndexPath = resolve(this.integrationDataFolder, 'testEntity3', 'index.json');

    // Test Entity 1
    {
      if (!existsSync(testEntity1IndexPath)) {
        expect.toBeTrue(false, `${testEntity1IndexPath} does not exists`);
      }

      const content = await readFile(testEntity1IndexPath, { encoding: 'utf-8' });

      expect.toBeDefined(content);
      expect.toBeGreater(content.length, 0, `${testEntity1IndexPath} has content`);

      const testEntity1 = await db.testEntities1.create({
        test: 'test123',
      });
      const testEntity1Path = resolve(this.integrationDataFolder, 'testEntity1', 'objects', `${testEntity1.id}.json`);
      expect.toBeTrue(
        existsSync(resolve(this.integrationDataFolder, 'testEntity1', 'objects', `${testEntity1.id}.json`)),
        `${testEntity1Path} does not exist`
      );

      testEntity1.test = 'test321';
      await db.testEntities1.update(testEntity1.id, testEntity1);

      const freshTestEntity1 = await db.testEntities1.get(testEntity1.id);
      expect.toBeEqual(freshTestEntity1.test, 'test321');

      await db.testEntities1.delete(testEntity1.id);

      expect.toBeFalse(
        existsSync(resolve(this.integrationDataFolder, 'testEntity1', 'objects', `${testEntity1.id}.json`)),
        `${testEntity1Path} does exist`
      );
    }

    // Test Entity 2
    {
      if (!existsSync(testEntity2IndexPath)) {
        expect.toBeTrue(false, `${testEntity2IndexPath} does not exists`);
      }

      const content = await readFile(testEntity2IndexPath, { encoding: 'utf-8' });

      expect.toBeDefined(content);
      expect.toBeGreater(content.length, 0, `${testEntity2IndexPath} has content`);

      const entity1 = await db.testEntities2.create({
        test: 1,
      });
      const entity2 = await db.testEntities2.create({
        test: 2,
      });
      const entity3 = await db.testEntities2.create({
        test: 3,
      });

      const indexedValues = await db.testEntities2.pluck('test');

      expect.toBeEqual(indexedValues[0], 1);
      expect.toBeEqual(indexedValues[1], 2);
      expect.toBeEqual(indexedValues[2], 3);

      const testEntity2Index: IndexFile<TestEntity2> = await readJson(testEntity2IndexPath);
      const expectedIndex = {
        count: 3,
        indexedProperties: ['id', 'test'],
        buckets: [
          ['id', [entity1.id, entity2.id, entity3.id].sort().map(id => [id, [id]])],
          [
            'test',
            [
              [1, [entity1.id]],
              [2, [entity2.id]],
              [3, [entity3.id]],
            ],
          ],
        ],
      };

      expect.toBeEqual(testEntity2Index.count, expectedIndex.count);
      expect.toBeEqual(testEntity2Index.indexedProperties[0], expectedIndex.indexedProperties[0]);
      expect.toBeEqual(testEntity2Index.buckets[0][0], expectedIndex.buckets[0][0]);
      expect.toBeEqual(testEntity2Index.buckets[0][1][0][0], expectedIndex.buckets[0][1][0][0]);
      expect.toBeEqual(testEntity2Index.buckets[0][1][0][1][0], expectedIndex.buckets[0][1][0][1][0]);
      expect.toBeEqual(testEntity2Index.buckets[0][1][1][0], expectedIndex.buckets[0][1][1][0]);
      expect.toBeEqual(testEntity2Index.buckets[0][1][1][1][0], expectedIndex.buckets[0][1][1][1][0]);
      expect.toBeEqual(testEntity2Index.buckets[0][1][2][0], expectedIndex.buckets[0][1][2][0]);
      expect.toBeEqual(testEntity2Index.buckets[0][1][2][1][0], expectedIndex.buckets[0][1][2][1][0]);

      expect.toBeEqual(testEntity2Index.buckets[1][0], expectedIndex.buckets[1][0]);
      expect.toBeEqual(testEntity2Index.buckets[1][1][0][0], expectedIndex.buckets[1][1][0][0]);
      expect.toBeEqual(testEntity2Index.buckets[1][1][0][1][0], expectedIndex.buckets[1][1][0][1][0]);
      expect.toBeEqual(testEntity2Index.buckets[1][1][1][0], expectedIndex.buckets[1][1][1][0]);
      expect.toBeEqual(testEntity2Index.buckets[1][1][1][1][0], expectedIndex.buckets[1][1][1][1][0]);
      expect.toBeEqual(testEntity2Index.buckets[1][1][2][0], expectedIndex.buckets[1][1][2][0]);
      expect.toBeEqual(testEntity2Index.buckets[1][1][2][1][0], expectedIndex.buckets[1][1][2][1][0]);
    }

    // Test Entity 3
    {
      if (!existsSync(testEntity3IndexPath)) {
        expect.toBeTrue(false, `${testEntity3IndexPath} does not exists`);
      }

      const content = await readFile(testEntity3IndexPath, { encoding: 'utf-8' });

      expect.toBeDefined(content);
      expect.toBeGreater(content.length, 0, `${testEntity3IndexPath} has content`);

      const expectedDate = new Date();

      const entity1 = await db.testEntities3.create({
        test: true,
        date: expectedDate,
      });
      const entity2 = await db.testEntities3.create({
        test: false,
        date: expectedDate,
      });
      const entity3 = await db.testEntities3.create({
        test: true,
        date: expectedDate,
      });

      db.testEntities3.invalidateCache();

      const freshEntity1 = await db.testEntities3.get(entity1.id);

      expect.toBeTrue(freshEntity1.date instanceof Date, 'should be instance of date');
      expect.toBeEqual(expectedDate.toISOString(), freshEntity1.date.toISOString());
    }
  }

  @Test('readme example')
  async readmeExample() {
    const dataFolder = this.exampleDataFolder;

    interface Order extends SpatiumEntity {
      orderDate: Date;
    }

    interface Customer extends SpatiumEntity {
      name: string;
      lastSeen: Date;
      age: number;
    }

    interface Invoice extends SpatiumEntity {
      amount: number;
    }

    class MyDatabase extends SpatiumDatabase {
      public orders = this.getTable<Order>('orders', ['id', 'orderDate'], {
        orderDate: SPATIUM_DATE_TRANSFORMER,
      });
      public customers = this.getTable<Customer>('customers', ['id', 'lastSeen', 'name', 'age'], {
        lastSeen: SPATIUM_DATE_TRANSFORMER,
      });
      public invoices = this.getTable<Invoice>('invoices', ['id']);

      constructor() {
        super(dataFolder);
      }
    }

    const db = new MyDatabase();

    await db.ensureTables();

    const johnDoe = await db.customers.create({
      name: 'John Doe',
      lastSeen: new Date(),
      age: 25,
    });

    await db.customers.create({
      name: 'Jane Doe',
      lastSeen: new Date(),
      age: 25,
    });

    await db.customers.update(johnDoe.id, {
      lastSeen: new Date(),
    });

    const order1 = await db.orders.create({
      orderDate: new Date(),
    });

    // Get all customers generator
    for (const customerPromise of await db.customers.all()) {
      const customer = await customerPromise;
    }

    const customers = await db.customers.collectAll();
    expect.toBeEqual(customers.length, 2, '2 customers');

    const anyJohnDoes = await db.customers.any(customer => customer.name === 'John Doe');
    expect.toBeTrue(anyJohnDoes, 'There should be a John Doe');

    const customerCount = await db.customers.count();
    expect.toBeEqual(customerCount, 2, '2 customers');

    await db.orders.delete(order1.id);

    const johnDoesByPredicate = await db.customers.find(customer => customer.name === 'John Doe');
    expect.toBeDefined(johnDoesByPredicate);
    expect.toBeEqual(johnDoesByPredicate.length, 1, '1 John Doe');
    expect.toBeEqual('John Doe', johnDoesByPredicate[0].name);

    const johnDoeById = await db.customers.get(johnDoe.id);
    expect.toBeDefined(johnDoeById);
    expect.toBeEqual('John Doe', johnDoeById.name);

    const noJackDoes = await db.customers.none(customer => customer.name === 'Jack Doe');
    expect.toBeTrue(noJackDoes);

    const customerNames = await db.customers.pluck('name');
    expect.toBeEqual(customerNames[0], 'Jane Doe');
    expect.toBeEqual(customerNames[1], 'John Doe');

    const customerIdsWhereAgeIs25 = await db.customers.pluckWhere('id', 'age', 25);
    expect.toBeEqual(customerIdsWhereAgeIs25.length, 2);

    const customersWhereNameContainsDoe = await db.customers.search('Doe', 'name');
    expect.toBeEqual(customersWhereNameContainsDoe.length, 2);

    const customersWhereNameIsJohnDoe = await db.customers.searchStrictly('John Doe', 'name');
    expect.toBeEqual(customersWhereNameIsJohnDoe.length, 1);

    const customersAge25WhereNameContainsJohn = await db.customers.searchWhere('age', 25, 'John', 'name');
    expect.toBeEqual(customersAge25WhereNameContainsJohn.length, 1);

    const customersAge25 = await db.customers.where('age', 25);
    expect.toBeEqual(customersAge25.length, 2);
  }

  @Test('benchmark')
  @Timeout(60000)
  async benchmark() {
    const benchmarkFolder = this.benchmarkFolder;
    const numberOfEntities = 2000;

    interface BenchmarkEntity extends SpatiumEntity {
      str: string;
      bool: boolean;
      number: number;
      date: Date;
      null: null;
    }

    const testEntity: BenchmarkEntity = {
      str: 'test',
      bool: true,
      number: 0,
      date: new Date(),
      null: null,
    };
    const testEntities = range(0, numberOfEntities).map(() => ({
      ...cloneDeep(testEntity),
      number: parseInt((Math.random() * 1000).toString(), 10),
      bool: Math.round(Math.random() * 10) > 5,
    }));

    class BenchmarkDatabase extends SpatiumDatabase {
      public benchmarks = this.getTable<BenchmarkEntity>('benchmarks', ['id', 'str', 'bool', 'number', 'date'], {
        date: SPATIUM_DATE_TRANSFORMER,
      });

      constructor() {
        super(benchmarkFolder);
      }
    }

    const db = new BenchmarkDatabase();

    await this.runBenchmark('ensureTables', () => db.ensureTables());
    await this.runBenchmark(
      'create',
      async () => {
        for (const entity of testEntities) {
          await db.benchmarks.create(entity, false);
        }
      },
      numberOfEntities
    );
    await this.runBenchmark('reindex', () => db.benchmarks.reindex());
    await this.runBenchmark('search', () => db.benchmarks.search(1, 'number'));
    await this.runBenchmark('searchWhere', () => db.benchmarks.searchWhere('bool', true, 1, 'number'));
    await this.runBenchmark('searchStrictly', () => db.benchmarks.searchStrictly(5, 'number'));
    await this.runBenchmark('pluck', () => db.benchmarks.pluck('date'));
    await this.runBenchmark('pluckWhere', () => db.benchmarks.pluckWhere('date', 'bool', true));
    await this.runBenchmark('any', () => db.benchmarks.any(b => b.bool === true && b.number === 5));
    await this.runBenchmark('none', () => db.benchmarks.none(b => b.bool === true && b.number === 5));
    await this.runBenchmark('create', () => db.benchmarks.create(testEntities[1000]));
  }

  private async runBenchmark<T>(marker: string, run: () => Promise<T>, averageDivider?: number) {
    const startEnsure = new Date();
    await run();
    const stopEnsure = new Date();

    this.logResult(marker, startEnsure, stopEnsure, averageDivider);
  }

  private logResult(marker: string, start: Date, stop: Date, averageDivider?: number) {
    const total = stop.getTime() - start.getTime();
    if (averageDivider !== undefined && averageDivider > 0) {
      console.log(
        `${marker} (${averageDivider} iterations) - ${total}ms and on average ${total / averageDivider}ms per iteration`
      );
    } else {
      console.log(`${marker} - ${total}ms`);
    }
  }
}
