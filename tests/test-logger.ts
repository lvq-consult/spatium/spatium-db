import { DatabaseLogger } from '../src/database-logger.interface';

class TestLogger implements DatabaseLogger {
  public logs: {
    method: 'log' | 'warn' | 'info' | 'error';
    args: any[];
  }[] = [];

  log(...args: any[]): void {
    // console.log('log', ...args);

    this.logs.push({
      method: 'log',
      args,
    });
  }
  warn(...args: any[]): void {
    // console.log('warn', ...args);

    this.logs.push({
      method: 'warn',
      args,
    });
  }
  info(...args: any[]): void {
    // console.log('info', ...args);

    this.logs.push({
      method: 'info',
      args,
    });
  }
  error(...args: any[]): void {
    // console.log('error', ...args);

    this.logs.push({
      method: 'error',
      args,
    });
  }
}

export const logger = new TestLogger();
