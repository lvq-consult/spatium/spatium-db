import { SpatiumDatabase } from '../src';
import { testFileService } from './test-fileservice';
import { logger } from './test-logger';

export interface TestEntity {
  test: string;
  obj: {
    test1: number;
  };
}

export class TestDatabase extends SpatiumDatabase {
  public get testTable() {
    return this.getTable('test');
  }

  constructor() {
    super('', logger, testFileService);
  }
}
