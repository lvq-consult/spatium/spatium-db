export interface DatabaseLogger {
  log(...args: any[]): void;
  warn(...args: any[]): void;
  info(...args: any[]): void;
  error(...args: any[]): void;
}
