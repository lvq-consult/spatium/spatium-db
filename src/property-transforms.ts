import { BaseRowEntity } from './base-row-entity';

export type PropertyTransform<TRowEntity extends BaseRowEntity, TKey extends keyof TRowEntity> = {
  fromStorage: (property: string | null | undefined) => TRowEntity[TKey];
  toStorage: (property: TRowEntity[TKey] | null | undefined) => string;
};

export type PropertyTransforms<TRowEntity extends BaseRowEntity> = {
  [key in keyof TRowEntity]?: PropertyTransform<TRowEntity, key>;
};
