import { BaseRowEntity } from './base-row-entity';
import { DatabaseLogger } from './database-logger.interface';
import { FileService } from './file.service';
import { PropertyTransforms } from './property-transforms';
import { Table } from './table';
import { TableIndex } from './table-index';

export abstract class Database {
  private tables = new Map<string, Table<any>>();

  constructor(private baseDataFolder: string, private logger?: DatabaseLogger, private fileService?: FileService) {
    this.fileService ??= new FileService(this.baseDataFolder, this.logger);
  }

  public async ensureTables() {
    this.logger?.info(`[database.ensureTables()] Ensuring ${this.tables.size} tables exists`);

    await Promise.all([...this.tables.values()].map(t => t.ensureExists()));
  }

  public getTable<TRowEntity extends BaseRowEntity>(
    name: string,
    indexedProperties: (keyof TRowEntity)[] = [],
    propertyTransforms: PropertyTransforms<TRowEntity> = {},
    tableIndex?: TableIndex<TRowEntity>
  ) {
    if (this.tables.has(name)) {
      this.logger?.info(`[database.getTable()] Table '${name}' already exists`);

      return this.tables.get(name) as Table<TRowEntity>;
    }

    this.logger?.info(`[database.getTable()] Table '${name}' does not exist`);

    const table = new Table<TRowEntity>(
      name,
      indexedProperties?.sort() ?? [],
      propertyTransforms,
      this.fileService,
      this.logger,
      tableIndex
    );
    this.tables.set(name, table);

    return table;
  }

  public getTables() {
    return [...this.tables.values()];
  }
}
