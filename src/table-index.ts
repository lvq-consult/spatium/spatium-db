import { isEqual } from 'lodash';
import { BaseRowEntity } from './base-row-entity';
import { DatabaseLogger } from './database-logger.interface';
import { FileService } from './file.service';
import { IndexFile, IndexHandler } from './index-handler';

export class TableIndex<TRowEntity extends BaseRowEntity> {
  private indexHandler: IndexHandler<TRowEntity>;
  private _filename: string;
  private get filename() {
    return (this._filename ??= `${this.tableName}/index.json`);
  }

  constructor(
    private tableName: string,
    private indexedProperties: (keyof TRowEntity)[] = [],
    private fileService: FileService,
    private logger?: DatabaseLogger
  ) {
    this.indexedProperties = this.indexedProperties.sort();
  }

  public async load() {
    try {
      const indexFile = await this.fileService.read(this.filename, {
        indexedProperties: this.indexedProperties,
        buckets: [],
        count: 0,
      } as IndexFile<TRowEntity>);

      let shouldWriteToDisk = false;

      if (!isEqual(indexFile.indexedProperties, this.indexedProperties)) {
        indexFile.indexedProperties = this.indexedProperties;
        shouldWriteToDisk = true;
      }

      this.indexHandler = new IndexHandler<TRowEntity>(indexFile);

      if (shouldWriteToDisk) {
        await this.writeIndexToDisk();
      }
    } catch (e) {
      process.exit();
    }
  }

  public async ensureExists() {
    if (this.fileService.exists(this.filename) === false) {
      this.logger?.info(`[table-index][${this.tableName}] index file for table does not exist`);
      await this.fileService.ensureExists(this.filename, {
        indexedProperties: this.indexedProperties,
        buckets: [],
        count: 0,
      } as IndexFile<TRowEntity>);
    }
  }

  public getIndexedValues<TKey extends keyof TRowEntity>(key: TKey): TRowEntity[TKey][] {
    return this.indexHandler.getIndexedValues(key);
  }

  public searchByProperty<TProperty extends keyof TRowEntity>(
    searchValue: TRowEntity[TProperty],
    ...properties: TProperty[]
  ) {
    const searchResult = this.indexHandler.searchByProperty(searchValue, ...properties);

    return searchResult;
  }

  public searchStrictlyByProperty<TProperty extends keyof TRowEntity>(
    searchValue: TRowEntity[TProperty],
    ...properties: TProperty[]
  ) {
    const searchResult = this.indexHandler.searchStrictlyByProperty(searchValue, ...properties);

    return searchResult;
  }

  public async index(row: TRowEntity) {
    this.logger?.info(`[table-index][${this.tableName}][${row.id}] Indexing`);
    this.indexHandler.writeToIndex(row);

    return this.writeIndexToDisk();
  }

  public async deindex(row: TRowEntity) {
    this.indexHandler.removeFromIndex(row);

    return this.writeIndexToDisk();
  }

  public async destroy() {
    this.indexHandler = new IndexHandler<TRowEntity>({
      indexedProperties: this.indexedProperties,
      buckets: [],
      count: 0,
    });
    await this.fileService.delete(this.filename);
  }

  private async writeIndexToDisk() {
    return await this.fileService.write(this.filename, this.indexHandler.serialize());
  }
}
