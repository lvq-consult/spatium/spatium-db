import { ensureFile, existsSync, readJson, remove, writeJson } from 'fs-extra';
import { sep } from 'path';
import { DatabaseLogger } from './database-logger.interface';
import { FileHandler } from './file-handler.interface';

export class JsonFileHandler<TSchema> implements FileHandler<TSchema> {
  private cache: TSchema;
  private get relativePath() {
    const pathSplit = this.path.split('/');

    return pathSplit.slice(pathSplit.length - 4, pathSplit.length).join(sep);
  }

  constructor(private path: string, private defaultValue?: TSchema, private logger?: DatabaseLogger) {
    this.log('created file handler');
  }

  public async write(data: TSchema) {
    this.log('writing');
    await this.ensureExists();
    await writeJson(this.path, data, { encoding: 'utf-8', spaces: 2 });
    this.cache = undefined;
  }

  public async read() {
    this.log('trying to read');

    if (this.cache) {
      this.log('serving cache');

      return this.cache;
    }

    this.log('reading');

    if (!this.exists()) {
      this.log('can not read, file does not exist');

      return this.defaultValue ?? undefined;
    }

    const value = (this.cache = await readJson(this.path, { encoding: 'utf-8' })) as TSchema;

    return this.defaultValue ? value ?? this.defaultValue : value;
  }

  public async delete() {
    this.log('deleting');
    this.cache = undefined;
    await remove(this.path);
  }

  public exists() {
    this.log('check if exists');

    return existsSync(this.path);
  }

  public async ensureExists() {
    if (!this.exists()) {
      this.log('does not exist');

      await ensureFile(this.path);

      if (this.defaultValue) {
        await writeJson(this.path, this.defaultValue, { encoding: 'utf-8', spaces: 2 });
      }
    }
  }

  private log(message: string) {
    this.logger?.info(`[filehandler][${this.relativePath}] ${message}`);
  }
}
