import { distinctFilter } from './utils/distinct-filter.fn';
import { stringComparer } from './utils/string-comparer.fn';
import { BaseRowEntity } from './base-row-entity';

export type BucketValue<TRowEntity extends BaseRowEntity> = [TRowEntity[keyof TRowEntity], TRowEntity['id'][]];

export type Bucket<TRowEntity extends BaseRowEntity> = [keyof TRowEntity, BucketValue<TRowEntity>[]];

export type BucketMapValue<TRowEntity extends BaseRowEntity> = Map<TRowEntity[keyof TRowEntity], TRowEntity['id'][]>;
export type KeyedBucketMapValue<TRowEntity extends BaseRowEntity, TKey extends keyof TRowEntity> = Map<
  TRowEntity[TKey],
  TRowEntity['id'][]
>;

export type BucketMap<TRowEntity extends BaseRowEntity> = Map<keyof TRowEntity, BucketMapValue<TRowEntity>>;

export interface IndexFile<TRowEntity extends BaseRowEntity> {
  indexedProperties: (keyof TRowEntity)[];
  buckets: Bucket<TRowEntity>[];
  count: number;
}

export interface IndexState<TRowEntity extends BaseRowEntity> {
  indexedProperties: (keyof TRowEntity)[];
  buckets: BucketMap<TRowEntity>;
  count: number;
}

export class IndexHandler<TRowEntity extends BaseRowEntity> {
  private state: IndexState<TRowEntity> = { buckets: new Map(), indexedProperties: [], count: 0 };

  constructor(private initialState: IndexFile<TRowEntity>) {
    this.state.indexedProperties = this.initialState?.indexedProperties ?? [];
    this.state.buckets = this.parseBuckets(this.initialState?.buckets ?? []);
    this.state.count = this.initialState?.count ?? 0;
  }

  public serialize() {
    const indexedProperties = this.state.indexedProperties;
    const buckets = this.serializeBuckets();
    const count = this.state.count;

    return { count, indexedProperties, buckets } as IndexFile<TRowEntity>;
  }

  public searchStrictlyByProperty<TKey extends keyof TRowEntity>(
    searchValue: TRowEntity[TKey],
    ...properties: TKey[]
  ): TRowEntity['id'][] {
    if (!searchValue) {
      return [];
    }
    const stringifiedSearchValue = searchValue.toString();
    const result: TRowEntity['id'][] = [];

    for (const property of properties) {
      if (!this.state.indexedProperties.includes(property)) {
        throw new Error(`'${property}'-property is not indexed and not searchable.`);
      }

      const bucket = this.getBucket(property);

      for (const [value, ids] of bucket) {
        const stringifiedValue = value.toString();

        if (stringifiedValue === stringifiedSearchValue) {
          result.push(...ids);
        }
      }
    }

    return result.filter(distinctFilter).sort();
  }

  public searchByProperty<TKey extends keyof TRowEntity>(
    searchValue: TRowEntity[TKey],
    ...properties: TKey[]
  ): TRowEntity['id'][] {
    if (!searchValue) {
      return [];
    }
    const stringifiedSearchValue = searchValue.toString().toLowerCase();
    const result: TRowEntity['id'][] = [];

    for (const property of properties) {
      if (!this.state.indexedProperties.includes(property)) {
        throw new Error(`'${property}'-property is not indexed and not searchable.`);
      }

      const bucket = this.getBucket(property);

      for (const [value, ids] of bucket) {
        const stringifiedValue = value.toString().toLowerCase();

        if (stringifiedValue.includes(stringifiedSearchValue)) {
          result.push(...ids);
        }
      }
    }

    return result.filter((v, idx, self) => self.indexOf(v) === idx).sort();
  }

  public getBucket<TKey extends keyof TRowEntity>(key: TKey) {
    if (!this.state?.buckets?.has(key)) {
      this.state.buckets.set(key, new Map());
    }

    return (this.state?.buckets?.get(key) ?? new Map()) as KeyedBucketMapValue<TRowEntity, TKey>;
  }

  public writeToIndex(entity: TRowEntity) {
    const properties = (Object.keys(entity) as (keyof TRowEntity)[]).filter(key => this.shouldIndexProperty(key));

    this.state.count += 1;

    for (const property of properties) {
      const bucket = this.getBucket(property);
      const value = entity[property];

      if (!bucket.has(value)) {
        bucket.set(value, []);
      }

      const ids = bucket.get(value);

      if (!ids.includes(entity.id)) {
        ids.push(entity.id);

        bucket.set(value, ids.sort());
      }
    }
  }

  public removeFromIndex(entity: TRowEntity) {
    const properties = (Object.keys(entity) as (keyof TRowEntity)[]).filter(key => this.shouldIndexProperty(key));

    this.state.count -= 1;

    for (const property of properties) {
      const bucket = this.getBucket(property);
      const value = entity[property];

      if (!bucket.has(value)) {
        bucket.set(value, []);
      }

      const ids = bucket.get(value);

      if (ids.includes(entity.id)) {
        const idx = ids.indexOf(entity.id);

        bucket.set(value, ids.splice(idx, 1).sort());
      }
    }
  }

  public getIndexedValues<TKey extends keyof TRowEntity>(key: TKey): TRowEntity[TKey][] {
    const bucket = this.getBucket(key);

    return [...bucket.keys()].sort();
  }

  private shouldIndexProperty(key: keyof TRowEntity) {
    return this.state?.indexedProperties?.includes(key) ?? false;
  }

  private parseBuckets(buckets: Bucket<TRowEntity>[]): BucketMap<TRowEntity> {
    if (!buckets?.length) {
      return new Map();
    }

    const result: BucketMap<TRowEntity> = new Map();

    for (const [property, values] of buckets) {
      if (!result.has(property)) {
        result.set(property, new Map());
      }

      const current = result.get(property);

      for (const [value, ids] of values) {
        current.set(value, ids);
      }
    }

    return result;
  }

  private serializeBuckets() {
    const buckets: Bucket<TRowEntity>[] = [];

    for (const [property, values] of this.state.buckets.entries()) {
      const bucket: Bucket<TRowEntity> = [
        property,
        [...values.entries()].sort(([valueA], [valueB]) => stringComparer(valueA.toString(), valueB.toString())),
      ];

      buckets.push(bucket);
    }

    return buckets.sort(([propertyA], [propertyB]) => stringComparer(propertyA.toString(), propertyB.toString()));
  }
}
