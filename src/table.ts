import { mkdirp } from 'fs-extra';
import * as glob from 'glob';
import { basename, resolve as resolvePath } from 'path';
import { v4 as uuidv4 } from 'uuid';
import { BaseRowEntity } from './base-row-entity';
import { DatabaseLogger } from './database-logger.interface';
import { FileService } from './file.service';
import { PropertyTransforms } from './property-transforms';
import { Row } from './row';
import { TableIndex } from './table-index';
import { distinctFilter } from './utils/distinct-filter.fn';

export class Table<TRowEntity extends BaseRowEntity> {
  private cachedRows = new Map<string, Row<TRowEntity>>();

  constructor(
    public tableName: string,
    public indexedProperties: (keyof TRowEntity)[] = [],
    private propertyTransforms: PropertyTransforms<TRowEntity> = {},
    private fileService: FileService,
    private logger?: DatabaseLogger,
    private tableIndex?: TableIndex<TRowEntity>
  ) {
    this.tableIndex ??= new TableIndex(this.tableName, this.indexedProperties, this.fileService);
  }

  /**
   * Ensure the files for the table actually exists
   */
  public async ensureExists() {
    const objectFolderPath = this.fileService.getObjectsFolderPath(this.tableName);
    this.logger?.info(`[table.ensureExists()] mkdirp(${objectFolderPath})`);
    await mkdirp(objectFolderPath);
    await this.tableIndex.ensureExists();
    await this.tableIndex.load();
  }

  /**
   * Invalidates the entire row cache
   */
  public invalidateCache() {
    this.cachedRows = new Map<string, Row<TRowEntity>>();
  }

  /**
   * Invalidate specifc row cache entry
   */
  public invalidateCachedRow(id: string) {
    this.cachedRows.delete(id);
  }

  /**
   * Get a generator, that yields promises for completely async iteration
   */
  public async all() {
    const objectsPath = this.fileService.getObjectsFolderPath(this.tableName);
    const pattern = resolvePath(objectsPath, '*.json');
    const files = await new Promise<string[]>((resolve, reject) =>
      glob(pattern, (err, matches) => (err ? reject(err) : resolve(matches)))
    );
    const ids = files.map(filename => basename(filename, '.json'));
    const generator = await this.collectByIdsIterator(ids);

    return generator;
  }

  /**
   * Fuzzy search the table index for values containing the search value
   */
  public search<TProperty extends keyof TRowEntity>(searchValue: TRowEntity[TProperty], ...properties: TProperty[]) {
    const ids = this.tableIndex.searchByProperty(searchValue, ...properties);

    return this.collectByIds(ids);
  }

  /**
   * Search the table index for values that strictly equals the search value
   */
  public searchStrictly<TProperty extends keyof TRowEntity>(
    searchValue: TRowEntity[TProperty],
    ...properties: TProperty[]
  ) {
    const ids = this.tableIndex.searchStrictlyByProperty(searchValue, ...properties);

    return this.collectByIds(ids);
  }

  /**
   * Fuzzy search the index with strict where clause
   */
  public async searchWhere<TProperty extends keyof TRowEntity>(
    whereProperty: TProperty,
    whereValue: TRowEntity[TProperty],
    searchValue: TRowEntity[TProperty],
    ...properties: TProperty[]
  ) {
    const result: TRowEntity[] = [];

    for (const entity of await this.search(searchValue, ...properties)) {
      const current = entity;
      if (current[whereProperty] === whereValue) {
        result.push(current);
      }
    }

    return result.sort((a, b) => a.sequence - b.sequence);
  }

  /**
   * Runs through the generator provided by .all()
   */
  public async collectAll() {
    const result: TRowEntity[] = [];

    for (const entity of await this.all()) {
      const current = await entity;
      result.push(current);
    }

    return result.sort((a, b) => a.sequence - b.sequence);
  }

  /**
   * Count every row in the table
   */
  public async count() {
    const objectsPath = this.fileService.getObjectsFolderPath(this.tableName);
    const pattern = resolvePath(objectsPath, '*.json');
    const files = await new Promise<string[]>((resolve, reject) =>
      glob(pattern, (err, matches) => (err ? reject(err) : resolve(matches)))
    );

    return files.length;
  }

  /**
   * Where clause
   */
  public async where<TKey extends keyof TRowEntity>(key: TKey, value: TRowEntity[TKey]) {
    const result: TRowEntity[] = [];

    for (const entity of await this.all()) {
      const current = await entity;
      if (current[key] === value) {
        result.push(current);
      }
    }

    return result.sort((a, b) => a.sequence - b.sequence);
  }

  /**
   * Get all distinct values by key
   */
  public async pluck<TKey extends keyof TRowEntity>(key: TKey) {
    if (!this.indexedProperties.includes(key)) {
      this.logger?.warn(`key: ${key} was not an indexed property of table ${this.tableName} using slow pluck`);
      const entities = await this.collectAll();

      return entities
        .map(entity => entity[key])
        .filter(distinctFilter)
        .sort();
    }

    return this.getIndexedValues(key).sort();
  }

  /**
   * Get all distinct values by key and where clause
   */
  public async pluckWhere<TKey extends keyof TRowEntity, TSearchKey extends keyof TRowEntity>(
    key: TKey,
    searchKey: TSearchKey,
    value: TRowEntity[TSearchKey]
  ) {
    const result = await this.where(searchKey, value);

    return result
      .map(e => e[key])
      .filter(distinctFilter)
      .sort();
  }

  /**
   * Test if any entity matches predicate.
   * Uses the genrator underneath, and will return when the first match is found.
   */
  public async any(predicate: (entity: TRowEntity) => boolean) {
    for (const entity of await this.all()) {
      const current = await entity;

      if (predicate(current)) {
        return true;
      }
    }

    return false;
  }

  /**
   * Inverse of .any()
   * Uses the genrator underneath, and will return when the first match is found.
   */
  public async none(predicate: (entity: TRowEntity) => boolean) {
    for (const entity of await this.all()) {
      const current = await entity;

      if (predicate(current)) {
        return false;
      }
    }

    return true;
  }

  /**
   * Find entity by predicate.
   * Uses the genrator underneath, and will return when the first match is found.
   */
  public async find(predicate: (entity: TRowEntity) => boolean) {
    const result: TRowEntity[] = [];

    for (const entity of await this.all()) {
      const current = await entity;

      if (predicate(current)) {
        result.push(current);
      }
    }

    return result.sort((a, b) => a.sequence - b.sequence);
  }

  public async get(id: string) {
    const row = this.getRow(id);
    const entity = await row.read(this.propertyTransforms);

    return entity;
  }

  /**
   * Create a new row
   */
  public async create(entity: TRowEntity, writeToIndex = true) {
    const id = entity.id ?? uuidv4();
    await new Promise<void>(r => setTimeout(() => r(), 1));
    const sequence = entity.sequence ?? new Date().getTime();
    const data = {
      ...entity,
      id,
      sequence,
    } as TRowEntity;
    const row = this.newRow(data);

    await row.write(data, this.propertyTransforms);

    if (writeToIndex) {
      await this.tableIndex.index(data);
    }

    this.cachedRows.set(id, row);

    return data;
  }

  /**
   * Update a row with a patch
   */
  public async update(id: string, patch: Partial<TRowEntity>, writeToIndex = true) {
    const row = this.getRow(id);
    const existingData = await row.read(this.propertyTransforms);
    const data = {
      ...existingData,
      ...patch,
    } as TRowEntity;

    await row.write(data, this.propertyTransforms);

    if (writeToIndex) {
      await this.tableIndex.deindex(data);
      await this.tableIndex.index(data);
    }

    return data;
  }

  /**
   * Delete a row
   */
  public async delete(id: string) {
    const row = this.getRow(id);
    const entity = await row.read(this.propertyTransforms);

    await row.delete();
    await this.tableIndex.deindex(entity);
    this.cachedRows.delete(id);
  }

  /**
   * Reindex the table
   */
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  public async reindex(progress: () => void = () => {}, finished: () => void = () => {}) {
    await this.tableIndex.destroy();
    await this.tableIndex.ensureExists();
    await this.tableIndex.load();

    for (const entity of await this.all()) {
      const current = await entity;
      this.logger?.info(`[table][${this.tableName}][${current.id}] Reindexing`);
      await this.tableIndex.index(current);

      progress();
    }

    finished();
  }

  private getIndexedValues<TKey extends keyof TRowEntity>(key: TKey): TRowEntity[TKey][] {
    return this.tableIndex.getIndexedValues(key);
  }

  private async collectByIds(ids: TRowEntity['id'][]) {
    const result: TRowEntity[] = [];

    for (const id of ids) {
      const row = this.getRow(id);

      result.push(await row.read(this.propertyTransforms));
    }

    return result.sort((a, b) => a.sequence - b.sequence);
  }

  private async collectByIdsIterator(ids: TRowEntity['id'][]) {
    const _row = (id: string) => {
      return this.getRow(id);
    };

    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const self = this;

    return (function* generator() {
      for (const id of ids) {
        const row = _row(id);

        yield row.read(self.propertyTransforms);
      }
    })();
  }

  private newRow(entity: TRowEntity) {
    const row = new Row<TRowEntity>(this.tableName, entity.id, this.fileService, this.logger);

    return row;
  }

  private getRow(id: string) {
    this.garbageCollectCache();

    if (this.cachedRows.has(id)) {
      return this.cachedRows.get(id);
    } else {
      const row = new Row<TRowEntity>(this.tableName, id, this.fileService, this.logger);

      this.cachedRows.set(id, row);

      return row;
    }
  }

  /**
   * Deletes 100 keys from the cached rows
   */
  private garbageCollectCache() {
    if (this.cachedRows.size < 1000) {
      return;
    }

    const keys = [...this.cachedRows.keys()];

    for (let i = 900; i < 1000; i++) {
      const key = keys[i];
      this.cachedRows.delete(key);
    }
  }
}
