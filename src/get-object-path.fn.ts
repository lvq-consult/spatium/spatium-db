import { resolve } from 'path';

/**
 * @deprecated
 */
export function getObjectPath(dataFolder: string, tableName: string, id: string) {
  return resolve(dataFolder, tableName, 'objects', `${id}.json`);
}
