export interface BaseRowEntity {
  id?: string;
  sequence?: number;
}
