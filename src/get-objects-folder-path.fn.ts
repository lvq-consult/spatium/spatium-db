import { resolve } from 'path';

/**
 * @deprecated
 */
export function getObjectsFolderPath(dataFolder: string, tableName: string) {
  return resolve(dataFolder, tableName, 'objects');
}
