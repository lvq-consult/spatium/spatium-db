import { ensureFile, existsSync, readJson, remove, writeJson } from 'fs-extra';
import { resolve, sep } from 'path';
import { DatabaseLogger } from './database-logger.interface';

export class FileService {
  private cache = new Map<string, any>();

  constructor(private baseDataFolder: string, private logger?: DatabaseLogger) {}

  public getObjectPath(tableName: string, id: string) {
    return resolve(this.baseDataFolder, tableName, 'objects', `${id}.json`);
  }

  public getObjectsFolderPath(tableName: string) {
    return resolve(this.baseDataFolder, tableName, 'objects');
  }

  public async write<TSchema>(path: string, data: TSchema) {
    const p = this.prepandBaseFolderToPath(path);

    this.log('writing', p);
    await this.ensureExists(p);
    await writeJson(p, data, { encoding: 'utf-8', spaces: 2 });
    this.cache.delete(p);
  }

  public async read<TSchema>(path: string, defaultValue?: TSchema) {
    const p = this.prepandBaseFolderToPath(path);

    this.log('trying to read', p);

    if (this.cache.has(p)) {
      this.log('serving cache', p);

      return this.cache.get(p) as TSchema;
    }

    this.log('reading', p);

    if (!this.exists(p)) {
      this.log('can not read, file does not exist', p);

      return defaultValue ?? undefined;
    }

    const value = (await readJson(p, { encoding: 'utf-8' })) as TSchema;

    this.cache.set(p, value);

    return defaultValue ? value ?? defaultValue : value;
  }

  public async delete(path: string) {
    const p = this.prepandBaseFolderToPath(path);

    this.log('deleting', p);
    this.cache.delete(p);
    await remove(p);
  }

  public exists(path: string) {
    const p = this.prepandBaseFolderToPath(path);

    this.log('check if exists', p);

    return existsSync(p);
  }

  public async ensureExists<TSchema>(path: string, defaultValue?: TSchema) {
    const p = this.prepandBaseFolderToPath(path);

    if (!this.exists(p)) {
      this.log('does not exist', p);

      await ensureFile(p);

      if (defaultValue) {
        await writeJson(p, defaultValue, { encoding: 'utf-8', spaces: 2 });
      }
    }
  }

  private prepandBaseFolderToPath(path: string) {
    if (path.startsWith(resolve(this.baseDataFolder))) {
      return path;
    }

    return resolve(this.baseDataFolder, path);
  }

  private getRelativePath(path: string) {
    if (!path) {
      return '';
    }

    const pathSplit = path.split('/');

    return pathSplit.slice(pathSplit.length - 4, pathSplit.length).join(sep);
  }

  private log(message: string, path: string) {
    this.logger?.info(`[fileService][${this.getRelativePath(path ?? '')}] ${message}`);
  }
}
