export interface FileHandler<TSchema> {
  write(data: TSchema): Promise<void>;
  read(): Promise<TSchema>;
  delete(): Promise<void>;
  exists(): boolean;
  ensureExists(): Promise<void>;
}
