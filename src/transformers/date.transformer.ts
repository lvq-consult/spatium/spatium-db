export const SPATIUM_DATE_TRANSFORMER = {
  fromStorage: (property: string) => new Date(property),
  toStorage: (property: Date) => property.toISOString(),
};
