import { BaseRowEntity } from './base-row-entity';
import { DatabaseLogger } from './database-logger.interface';
import { FileService } from './file.service';
import { cloneDeep } from 'lodash';
import { PropertyTransforms, PropertyTransform } from './property-transforms';

export class Row<TRowEntity extends BaseRowEntity> {
  private get filename() {
    return this.fileService.getObjectPath(this.tableName, this.id);
  }

  constructor(
    private tableName: string,
    private id: string,
    private fileService: FileService,
    private logger?: DatabaseLogger
  ) {}

  public async read(propertyTransforms: PropertyTransforms<TRowEntity> = {}) {
    this.logger?.info(`[row][${this.id}] reading`);

    const obj = await this.fileService.read<unknown>(this.filename);

    const transforms = Object.keys(propertyTransforms);

    if (transforms.length > 0) {
      transforms
        .map(key => ({ transform: propertyTransforms[key] as PropertyTransform<TRowEntity, any>, key }))
        .forEach(({ transform, key }) => (obj[key] = transform.fromStorage(obj[key])));
    }

    return obj as TRowEntity;
  }

  public async write(entity: TRowEntity, propertyTransforms: PropertyTransforms<TRowEntity> = {}) {
    this.logger?.info(`[row][${this.id}] writing`);

    const transforms = Object.keys(propertyTransforms);

    if (transforms.length > 0) {
      const clone = cloneDeep(entity);

      transforms
        .map(key => ({ transform: propertyTransforms[key] as PropertyTransform<TRowEntity, any>, key }))
        .forEach(({ transform, key }) => (clone[key] = transform.toStorage(clone[key])));

      await this.fileService.write<TRowEntity>(this.filename, clone);
    } else {
      await this.fileService.write<TRowEntity>(this.filename, entity);
    }
  }

  public async delete() {
    this.logger?.info(`[row][${this.id}] deleting`);
    await this.fileService.delete(this.filename);
  }
}
