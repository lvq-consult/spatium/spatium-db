# Spatium DB

Spatium DB was created because we needed to have a simple file based database, that would work well with Git, thus having to be easily diffable.

The database is intended to be used alongside Typescript.

## Getting started

```typescript
/**
 * interface SpatiumEntity {
 *   id?: string;
 *   sequence?: number;
 * }
 */

import { SpatiumDatabase, SpatiumEntity, SPATIUM_DATE_TRANSFORMER } from '@lvqconsult/spatium-db';

interface Order extends SpatiumEntity {
  orderDate: Date;
}

interface Customer extends SpatiumEntity {
  name: string;
  lastSeen: Date;
}

interface Invoice extends SpatiumEntity {
  amount: number;
}

class MyDatabase extends SpatiumDatabase {
  public orders = this.getTable<Order>('orders', ['id', 'orderDate']);
  public customers = this.getTable<Customer>('customers', ['id', 'test']);
  public invoices = this.getTable<Invoice>('invoices', ['id', 'test']);

  constructor() {
    super(resolve(process.cwd(), './db'));
  }
}

const db = new MyDatabase();

await db.ensureTables();

const johnDoe = await db.customers.create({
  name: 'John Doe',
  lastSeen: new Date(),
  age: 25,
});

await db.customers.create({
  name: 'Jane Doe',
  lastSeen: new Date(),
  age: 25,
});

await db.customers.update(johnDoe.id, {
  lastSeen: new Date(),
});

const order1 = await db.orders.create({
  orderDate: new Date(),
});

// Get all customers generator
for (const customerPromise of await db.customers.all()) {
  const customer = await customerPromise;
}

const customers = await db.customers.collectAll();
const anyJohnDoes = await db.customers.any(customer => customer.name === 'John Doe');
const customerCount = await db.customers.count();
await db.orders.delete(order1.id);
const johnDoesByPredicate = await db.customers.find(customer => customer.name === 'John Doe');
const johnDoeById = await db.customers.get(johnDoe.id);
const noJackDoes = await db.customers.none(customer => customer.name === 'Jack Doe');
const customerNames = await db.customers.pluck('name');
const customerIdsWhereAgeIs25 = await db.customers.pluckWhere('id', 'age', 25);
const customersWhereNameContainsDoe = await db.customers.search('Doe', 'name');
const customersWhereNameIsJohnDoe = await db.customers.searchStrictly('John Doe', 'name');
const customersAge25WhereNameContainsJohn = await db.customers.searchWhere('age', 25, 'John', 'name');
const customersAge25 = await db.customers.where('age', 25);
```
